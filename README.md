# On Time - Administration Panel

### Law

[Spanish Law on punch clocks](https://boe.es/buscar/doc.php?id=BOE-A-2019-3481) (Chapter III Article 10)

### Server

Any LAMP server works well with this website.
For our server, we use Apache2, MariaDB and PHP7.

#### Install (Apache2)

```sh
# Clone the project
git clone https://gitlab.com/microhard/ontime-panel.git
cd ontime-panel

# Create a symlink from the src folder to /var/www/html
ln -s src /var/www/html

# Edit the file src/config.ini and ignore its changes
git update-index --skip-worktree src/config.ini

# Edit the apache2 vhost file to allow .htaccess modifications (AllowOverride all)
echo '<VirtualHost *:80>
	DocumentRoot /var/www/html
	<Directory /var/www/html>
		AllowOverride all
	</Directory>
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>' > /etc/apache2/sites-enabled/000-default.conf

# Enable the following Apache2 mods
a2enmod rewrite
a2enmod headers
systemctl reload apache2
```

#### Install (MariaDB)

Install from [microhard/ontime-database](https://gitlab.com/microhard/ontime-database).

### External Libraries and Resources Used

**PHPMailer**: [Repository](https://github.com/PHPMailer/PHPMailer) ([GNU LGPL v2.1](https://github.com/PHPMailer/PHPMailer/blob/master/LICENSE))

**Font Awesome**: [Website](https://fontawesome.com/) ([SIL OFL 1.1](https://fontawesome.com/license/free))

**Montserrat Font**: [Repository](https://github.com/JulietaUla/Montserrat) ([SIL OFL 1.1](https://github.com/JulietaUla/Montserrat/blob/master/OFL.txt))

**FPDF** [Website](http://www.fpdf.org/) ([Permisive License](https://github.com/Setasign/FPDF/blob/master/license.txt))
