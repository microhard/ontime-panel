<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";
	include __ROOT__."/lib/PDF.php";
	include __ROOT__."/utils/report.php";

	if (!isset($_SESSION["account"]))
		header("Location: /") and die();

	if (!isset($_GET["token"]))
		header("Location: /users") and die();

	$DB = new Database();
	$token = $DB->escape($_GET["token"]);
	$account_id = $_SESSION["account"]["id"];
	$user = $DB->query("SELECT * FROM user WHERE token = '$token' AND account_id = $account_id");

	if (!$user)
		header("Location: /users") and die();
	$user = $user[0];

	function download($range) {
		global $user;
		if (!preg_match('/\d{4}-\d{2}-\d{2};\d{4}-\d{2}-\d{2}/', $range))
			die();
		$range = explode(";", $range);
		$start = $range[0];
		$end = $range[1];
		$pdf = \report\generateReport($user, $start, $end);
		$start = date("F Y", strtotime($start));
		$end = date("F Y", strtotime($end));
		$month = $start === $end ? $start : "$start to $end";
		$pdf->Output("D", "$user[name] - $month - On Time.pdf") and die();
	}


	$alert = false;
	if (isset($_GET["download"])) {
		if (isset($_GET["range"]))
			download($_GET["range"]);
		elseif (isset($_GET["start"]) and isset($_GET["end"]))
			download("$_GET[start];$_GET[end]");
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $user["name"] ?> - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="stylesheet" type="text/css" href="/css/table.css">

	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">

	<script type="text/javascript" src="/js/dialog.js" defer></script>
	<script type="text/javascript" src="/js/request.js"></script>
</head>
<body>

	<main>
		<a href="/users" id="back">Go back to the user list</a>
		<h1><?php echo $user["name"] ?></h1>
		<?php if ($alert) echo "<code class='alert'>$alert</code>" ?> 
		<h2 class="s">Token: <span class="secret"><?php echo $user["token"] ?></span></h2>
		<div class="options">
			<?php
				$current_month  = date("F Y");
				$previous_month = date("F Y", strtotime("previous month"));

				$current_range = array(
					"start" => date("Y-m-d", strtotime("first day of this month")),
					"end" => date("Y-m-d", strtotime("last day of this month"))
				);
				$previous_range = array(
					"start" => date("Y-m-d", strtotime("first day of previous month")),
					"end" => date("Y-m-d", strtotime("last day of previous month"))
				);
			?> 
			<a href="/user/<?php echo $token ?>/download?range=<?php echo implode(";", $current_range) ?>" download>
				<i class="fa-solid fa-download"></i> Generate Report for <?php echo $current_month ?>
			</a>
			<a href="/user/<?php echo $token ?>/download?range=<?php echo implode(";", $previous_range) ?>" download>
				<i class="fa-solid fa-download"></i> Generate Report for <?php echo $previous_month ?>
			</a>
			<form action="<?php echo $_GET["token"] ?>/download" class="styless">
				<fieldset>
					<legend>Generate Report</legend>
					<label>From
						<input type="date" name="start" required>
					</label>
					<label>To
						<input type="date" name="end" required>
					</label>
					<button>Generate</button>
				</fieldset>
			</form>
		</div>
	</main>

</body>
</html>