<?php

include "fpdf/fpdf.php";

class PDF extends FPDF {

	private $pdf;
	
	function __construct($title = "On Time") {
		parent::__construct();
		$this->title = $title;
		$this->AddFont('Montserrat', '', 'MontserratAlternates-Regular.php');
		$this->AddFont('Montserrat', 'B', 'MontserratAlternates-Bold.php');
		$this->AliasNbPages();

		$this->AddPage();
		$this->SetFont("Montserrat", "", 11);
	}

	function Header() {
		$this->SetFont("Montserrat", "B", 16);
		$this->SetFillColor(0x59, 0x40, 0x32);
		$this->SetTextColor(0xFF, 0xFF, 0xFF);
		$this->SetXY(0, 5);
		$this->Cell(140, 10, $this->title, 0, 0, 'L', true);
		$this->SetXY(145, 5);
		$this->Cell(10, 10, "", 0, 0, 0, true);
		$this->SetXY(160, 5);
		$this->Cell(10, 10, "", 0, 0, 0, true);
		$this->SetXY(175, 5);
		$this->Cell(10, 10, "", 0, 0, 0, true);
		$this->Ln(20);
	}

	function Footer() {
		$this->SetFont("Montserrat", "", 11);
		$this->SetFillColor(0x59, 0x40, 0x32);
		$this->SetTextColor(0xFF, 0xFF, 0xFF);
		$this->SetXY(-140, -15);
		$timestamp = date("d/m/Y H:i:s");
		$page = $this->PageNo();
		$this->Cell(140, 10, "Generated at $timestamp        Page $page of {nb}         On Time", 0, 0, 'R', true);
		$this->SetXY(-155, -15);
		$this->Cell(10, 10, "", 0, 0, 0, true);
		$this->SetXY(-170, -15);
		$this->Cell(10, 10, "", 0, 0, 0, true);
		$this->SetXY(-185, -15);
		$this->Cell(10, 10, "", 0, 0, 0, true);
	}

}
