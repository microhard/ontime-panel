<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";
	include __ROOT__."/utils/mail/mail.php";
	include __ROOT__."/utils/report.php";

	if (!isset($_SESSION["account"]))
		header("Location: /") and die();


	$DB = new Database();

	function create($name, $email, $card) {
		global $DB;
		global $users;
		if (!$name or !$email)
			return "You need to fill the name and email fields for the user!";

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return "Invalid email!";

		do {
			$token = bin2hex(random_bytes(6)); // 6B = 48b -> 12 caràcters hexadecimals
		} while ($DB->query("SELECT token FROM user WHERE token = '$token'"));

		$name = $DB->escape($name);
		$email = $DB->escape($email);
		$card = $DB->escape($card);
		$account_id = $_SESSION["account"]["id"];

		$exists = $DB->query(
			"SELECT name FROM user WHERE card = '$card' AND account_id = $account_id"
		);
		if ($exists)
			return $exists[0]["name"]." is already using this card! Please choose another one.";

		$DB->query(
			"INSERT INTO user (token, name, email, card, account_id)
				VALUES ('$token', '$name', '$email', '$card', $account_id)"
		);

		array_unshift($users, array(
			"token" => $token,
			"name" => $name,
			"email" => $email,
			"card" => $card,
			"account_id" => $account_id
		));

		return "Created an user for $name!";
	}

	function delete($token) {
		global $DB;
		if (!$token)
			return "Please select the user to delete!";

		$token = $DB->escape($token);
		$account_id = $_SESSION["account"]["id"];

		$user = $DB->query(
			"SELECT name FROM user WHERE token = '$token' AND account_id = $account_id"
		);
		if (!$user)
			return "User not found!";
		$user_name = $user[0]["name"];

		$DB->query("DELETE FROM user WHERE token = '$token'");

		return "User <b>$user_name</b> was deleted!";
	}

	function modify($token, $name, $email, $card) {
		global $DB;
		if (!$token or !$name or !$email or !$card)
			return "Please fill all details!";

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return "Invalid email!";

		$token = $DB->escape($token);
		$name = $DB->escape($name);
		$email = $DB->escape($email);
		$card = $DB->escape($card);
		$account_id = $_SESSION["account"]["id"];

		$user = $DB->query(
			"SELECT name FROM user WHERE token = '$token' AND account_id = $account_id"
		);

		if (!$user)
			return "User not found!";

		$DB->query("UPDATE user SET name='$name', email='$email', card='$card' WHERE token = '$token'");

		return "User $name was updated!";
	}

	function send_instructions($token) {
		global $DB;
		if (!$token)
			return "Please select a user!";

		$token = $DB->escape($token);
		$account_id = $_SESSION["account"]["id"];

		$user = $DB->query(
			"SELECT name, email, token FROM user WHERE token = '$token' AND account_id = $account_id"
		);

		if (!$user)
			return "User not found!";
		$user = $user[0];

		$sent = \mail\send(
			"OnTime Manual",
			\mail\fill_template(
				\mail\TEMPLATE_INFORMATION,
				array(
					"user_name" => $user["name"],
					"token" => $user["token"]
				)
			),
			$user["email"]
		);

		if (!$sent)
			return "Could not send email!";

		return "Instructions sent!";
	}

	function send_report($token) {
		global $DB;
		if (!$token)
			return "Please select a user!";

		$token = $DB->escape($token);
		$account_id = $_SESSION["account"]["id"];

		$user = $DB->query(
			"SELECT name, email, token FROM user WHERE token = '$token' AND account_id = $account_id"
		);

		if (!$user)
			return "User not found!";
		$user = $user[0];

		$start = date("Y-m-d", strtotime("first day of this month"));
		$end = date("Y-m-d", strtotime("last day of this month"));
		$month = date("F Y");

		$report = \report\generateReport($user, $start, $end);
		$target = sys_get_temp_dir() . "/$user[token]";
		mkdir($target);
		$output = "$target/$user[name] - $month - On Time.pdf";
		$report->Output($output, 'F');

		$sent = \mail\send(
			"OnTime - Report for $month",
			\mail\fill_template(
				\mail\TEMPLATE_REPORT,
				array(
					"user_name" => $user["name"],
					"month" => $month
				)
			),
			$user["email"],
			$output
		);

		if (!$sent)
			return "Could not send email!";

		return "Report sent!";
	}


	$users = $DB->query(
		"SELECT * FROM user WHERE account_id = ".$_SESSION["account"]["id"]." ORDER BY name"
	);
	$users = $users ? $users : array();

	$alert = false;

	if (isset($_POST["create"]))
		$alert = create($_POST["name"], $_POST["email"], $_POST["card"]);
	elseif (isset($_POST["delete"]))
		die(delete($_POST["delete"]));
	elseif (isset($_POST["modify"]))
		die(modify($_POST["token"], $_POST["name"], $_POST["email"], $_POST["card"]));
	elseif (isset($_POST["send_instructions"]))
		die(send_instructions($_POST["send_instructions"]));
	elseif (isset($_POST["send_report"]))
		die(send_report($_POST["send_report"]));
	elseif (isset($_GET["download"]))
		switch ($_GET["download"]) {
			case "csv":
				header("Content-Type: text/csv");
				$csv = "token,name,email,card,account_id\n";
				foreach ($users as $user)
					$csv .= implode(",", $user)."\n";
				die($csv);
			
			case "json":
				header("Content-Type: application/json");
				die(json_encode($users));

			default:
				# code...
				break;
		}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Manage Users - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="stylesheet" type="text/css" href="/css/table.css">

	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">

	<script type="text/javascript" src="/js/dialog.js" defer></script>
	<script type="text/javascript" src="/js/request.js"></script>

	<script type="text/javascript" src="/js/users.js" defer></script>
</head>
<body>

	<form method="POST" action="/users">
		<?php if ($alert) echo "<code class='alert'>$alert</code>" ?>
		<input type="text" name="name" placeholder="Full Name" required>
		<input type="email" name="email" placeholder="Email" required>
		<input type="text" name="card" placeholder="card" required>
		<button name="create">
			<i class="fa-solid fa-user-plus"></i>
			Create a new User
		</button>
	</form>

	<main>
		<a href="/panel" id="back">Go back to the administration panel</a>
		<h1>Your Users</h1>
		<input type="text" placeholder="Search" id="search">
		<hr>
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Card</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody id="rows">
				<?php foreach ($users as $user): ?> 
					<tr id="row-token-<?php echo $user["token"] ?>">
						<td><?php echo $user["name"] ?></td>
						<td><?php echo $user["email"] ?></td>
						<td><?php echo $user["card"] ?></td>
						<td class="actions" data-user='<?php echo json_encode($user) ?>'>
							<i class="fa-solid fa-user user"></i>
							<i class="fa-solid fa-envelope email"></i>
							<i class="fa-solid fa-pen-to-square modify"></i>
							<i class="fa-solid fa-trash delete"></i>
						</td>
					</tr>
				<?php endforeach ?> 
			</tbody>
		</table>
		<hr>
		<div id="export">
			<a href="/users?download=csv" download="users.csv">
				<i class="fa-solid fa-file-csv"></i> Export as CSV
			</a>
			<a href="/users?download=json" download="users.json">
				<i class="fa-solid fa-file-code"></i> Export as JSON
			</a>
		</div>
	</main>

</body>
</html>