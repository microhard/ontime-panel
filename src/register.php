<?php

	require "app.php";
	include __ROOT__."/utils/mail/mail.php";
	include __ROOT__."/lib/Database.php";


	function register($email, $password) {
		if (!$email or !$password)
			return "Please enter your email and choose a safe password!";

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return "Invalid email!";

		if (strlen($password[0]) < 10)
			return "Password has to be longer than 10 characters!";

		if ($password[0] !== $password[1])
			return "Passwords didn't match!";

		$password = password_hash($password[0], PASSWORD_BCRYPT);

		$DB = new Database();
		$email = $DB->escape($email);
		$result = $DB->query("SELECT register('$email', '$password') as identifier");
		if (!$result)
			return "An error has occurred!";

		$identifier = CONFIG["server"]["name"].
			"login?confirm=".$result[0]["identifier"];

		$sent = \mail\send(
			"Please confirm your On Time registration",
			\mail\fill_template(
				\mail\TEMPLATE_REGISTER,
				array(
					"email" => $email,
					"link" => $identifier
				)
			),
			$email
		);

		if ($sent)
			return "<b>Account registered!</b><br>A confirmation email has been sent.";
		else
			return "<b>An error occurred!</b><br>Try again later.";
	}



	$alert = false;

	if (isset($_POST["register"]))
		$alert = register($_POST["email"], $_POST["password"]);


?>
<!DOCTYPE html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Register - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
</head>
<body>

	<form method="POST" action="/register">
		<?php if ($alert) echo "<code class='alert'>$alert</code>" ?>
		<small>
			Already have an account? <a href="/login">Log In</a> instead
		</small>
		<input type="email" name="email" placeholder="Email" required>
		<input type="password" name="password[]" placeholder="Passphrase" required>
		<input type="password" name="password[]" placeholder="Repeat Passphrase" required>
		<input type="submit" name="register" value="Register">
	</form>

	<main>
		<a href="/" id="back">Go back to the main page</a>
		<h1>Register your account</h1>
		<p>
			To start using the <b>On Time</b> Administration Panel, please enter your email on the registration form and confirm your identity by clicking on the link sent to your email. 
		</p>
		<p>
			If you are having any trouble with the registration process, please contact us.
		</p>
		<p>
			By using this page to register to the On Time Administration Panel, you agree with our <a href="/privacy">Privacy Policy</a>.
		</p>
		<img src="/img/timeclock.jpg" id="art">
	</main>

</body>
</html>