<?php

namespace mail;

require_once "app.php";
include __ROOT__."/lib/phpmailer/PHPMailer.php";
include __ROOT__."/lib/phpmailer/SMTP.php";


const TEMPLATE_REGISTER    = __ROOT__."/utils/mail/template/register.html";
const TEMPLATE_INFORMATION = __ROOT__."/utils/mail/template/information.html";
const TEMPLATE_REPORT      = __ROOT__."/utils/mail/template/report.html";
const TEMPLATE_SUPPORT     = __ROOT__."/utils/mail/template/support.html";


function send($subject, $body, $to, $attachment = null) {

	$mail = new \PHPMailer();
	$mail->isSMTP();
	$mail->Host        = CONFIG["email"]["host"];
	$mail->SMTPAuth    = true;
	$mail->SMTPSecure  = "tls";
	//$mail->SMTPAutoTLS = false;
	$mail->Username    = CONFIG["email"]["email"];
	$mail->Password    = CONFIG["email"]["password"];
	//$mail->SMTPSecure  = PHPMailer::ENCRYPTION_SMTPS; 
	$mail->Port        = CONFIG["email"]["port"];

	$mail->setFrom(CONFIG["email"]["email"], CONFIG["email"]["name"]);
	$mail->addAddress($to);
	$mail->addBCC(CONFIG["email"]["email"]);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->AltBody = strip_tags($body);

	if ($attachment)
		$mail->AddAttachment($attachment);

	try {
		return (bool) $mail->send();
		//$mail->ErrorInfo;
	} catch (Exception $e) {
		return false;
	}

}


function fill_template($template, $variables = null) {
	$content = file_get_contents($template);
	foreach ($variables as $key => $value)
		$content = str_replace("{{{$key}}}", $value, $content);
	return $content;
}
