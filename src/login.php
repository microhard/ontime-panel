<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";

	function login($email, $password) {
		if (!$email or !$password)
			return "Please enter your email and password!";

		$DB = new Database();
		$email = $DB->escape($email);
		$account = $DB->query("SELECT * FROM account WHERE email = '$email'");

		if (!$account)
			return "Incorrect login details!";

		$account = $account[0];

		if (!password_verify($password, $account["password"]))
			return "Incorrect login details!";

		$register = $DB->query("SELECT * FROM account_register WHERE id = $account[id]")[0];
		if ($register["status"] !== "confirmed")
			return "This account is not yet verified! Please check your email for the verification link.";

		$_SESSION["account"] = $account;
		header("Location: /panel");
		die();
	}

	function confirm($identifier) {
		$DB = new Database();
		$identifier = $DB->escape($identifier);
		$account_register = $DB->query(
			"SELECT * 
				FROM account_register 
				WHERE identifier = '$identifier'
				AND status = 'registered'"
		);

		if (!$account_register)
			return "This link is expired or invalid!";
		$account_register = $account_register[0];

		$DB->query(
			"UPDATE account_register 
				SET status = 'confirmed'
				WHERE id = $account_register[id]"
		);

		return "<b>Your account has been confirmed</b><br>You can now log in.";
	}

	$alert = false;

	if (isset($_POST["login"]))
		$alert = login($_POST["email"], $_POST["password"]);
	elseif (isset($_GET["logout"])) {
		unset($_SESSION["account"]);
		$alert = "Logged out successfully!";
	} elseif (isset($_SESSION["account"]))
		header("Location: /panel") and die();
	elseif (isset($_GET["confirm"]))
		$alert = confirm($_GET["confirm"]);

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Log In - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
</head>
<body>

	<form method="POST" action="/login">
		<?php if ($alert) echo "<code class='alert'>$alert</code>" ?> 
		<small>
			Don't have an account? <a href="/register">Register</a> instead
		</small>
		<input type="email" name="email" placeholder="Email" required>
		<input type="password" name="password" placeholder="Passphrase" required>
		<input type="submit" name="login" value="Log In">
	</form>

	<main>
		<a href="/" id="back">Go back to the main page</a>
		<h1>Log In</h1>
		<p>
			This is where you log in with your <b>account</b>. Every account has users inside. An user is the person that marks their working time.
		</p>
		<p>
			Very simple stuff if you ask us.
		</p>
		<img src="/img/clockblueprint.jpeg" id="art">
		<small class="copyright">
			Wellcome Collection, United Kingdom - CC BY.<br>
			https://www.europeana.eu/en/item/9200579/v75sufvc
		</small>
	</main>

</body>
</html>