<?php

	require "app.php";

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Manuals - On-Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />

	<link rel="stylesheet" type="text/css" href="/css/manuals.css">
	<script src="/js/manuals.js" defer></script>
</head>
<body>


	<main>
		<a href="/panel" id="back">Go back to the main page</a>

		<h1>Manuals</h1>

		<a id="white-paper" href="/doc/On Time White Paper.pdf" target="_blank">
			<img src="/img/white-paper-thumbnail.png">
			Download the white paper!
		</a>

		<div class="columns">
			<div class="manual">
				<h2>How to register</h2>
				<p>Please click register on the top right button
				and insert your e-mail and password or</p>
					<a id="buttonPDF"href="/doc/TutorialRegister.pdf">
						<i id="book" class="fa fa-book fa-sm"></i> Click here for a detailed explanation of the process.</a>
			</div>
		</div>

		<div class="columns">
			<div class="manual">
				<h2>How to manage Users</h2>
				<p>Please click on "Open tool" under manage users, you will be redirected to a new page where you can create, edit and delete users.</p>
					<a id="buttonPDF" href="/doc/TutorialManageUsers.pdf">
						<i id="book" class="fa fa-book fa-sm"></i> Click here for a detailed explanation of all functionalities</a>
			</div>
		</div>

		<div class="columns">
			<div class="manual">
				<h2>How to edit markings</h2>
				<p>Please click on "Open tool" under Edit Markings, you will be redirected to a new page where you can create, edit and delete markings.</p>
					<a id="buttonPDF" href="/doc/TutorialEditmarkings.pdf">
						<i id="book" class="fa fa-book fa-sm"></i> Click here for a detailed explanation of all functionalities.</a>
				</div>
			</div>
		</div>

		<div class="columns">
			<div class="manual">
				<h2>How to contact us</h2>
				<p>You can contact us by going to the administration panel and clicking on "Support page" under get help.</p>
					<a id="buttonPDF" href="/doc/TutorialContactUs.pdf">
						<i id="book" class="fa fa-book fa-sm"></i> Click here for a more detailed explanation.</a>
			</div>
		</div>
	</main>


</body>
</html>