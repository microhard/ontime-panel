
var filters = {
	user: null,
	year: null,
	month: null
}

var export_view = document.getElementById("export")

function loadrows(rows) {
	var table = document.getElementById("markings")

	var old_rows = table.querySelectorAll("tr.timecard")
	for (let old of old_rows)
		old.remove()

	if (rows) for (let row of rows) {
		let tr = document.createElement("tr")
		tr.innerHTML = `
			<td>${row.type}</td>
			<td>${row.timestamp}</td>
			<td class='actions' data-timecard='${JSON.stringify(row)}'>
				<i class="fa-solid fa-pen-to-square modify"></i>
				<i class="fa-solid fa-trash delete"></i>
			</td>
		`
		tr.id = "row-id-"+row.id
		tr.className = "timecard"
		table.appendChild(tr)
	}
}

function deleteTimecard(id) {
	request("POST", "/editor", "delete="+id, function(response) {
		dialog(response)
		document.getElementById("row-id-"+id).remove()
	})
}
function deleteTimecardDialog(timecard) {
	dialog(
		`Do you really want to delete this marking?`,
		[
			{"name": "Accept", "color": "#A3E183", "action": function(){deleteTimecard(timecard.id)}},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function modifyTimecard(timecard) {
	let urlencode = new URLSearchParams(timecard).toString()
	request("POST", "/editor", urlencode+"&modify=", function(response) {
		dialog(response)
		let row = document.getElementById("row-id-"+timecard.id)
		row.children[0].innerHTML = timecard.type
		row.children[1].innerHTML = timecard.timestamp
	})
}
function modifyTimecardDialog(timecard) {
	let out = timecard.type == "OUT" ? "selected" : ""
	let timestamp = timecard.timestamp.split(" ")
	dialog(
		`<div style="white-space: nowrap">
		<input type="hidden" name="id" value="${timecard.id}" required>
		<select name="type">
			<option value="IN">IN</option>
			<option value="OUT" ${out}>OUT</option>
		</select>
		<input type="date" name="date" value="${timestamp[0]}" required>
		<input type="time" name="time" value="${timestamp[1]}" required>
		<input type="hidden" name="user_token" value="${timecard.user_token}" required>
		</div>`,
		[
			{"name": "Modify", "color": "#A3E183", "action": function(m) {
				let date = m.querySelector("[name=date]").value
				let time = m.querySelector("[name=time]").value
				let timecard = {
					"id": m.querySelector("[name=id]").value,
					"type": m.querySelector("[name=type]").value,
					"timestamp": `${date} ${time}`,
					"user_token": m.querySelector("[name=user_token]").value
				}
				modifyTimecard(timecard)
			}},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function reload() {
	request(
		"GET",
		`/editor?user=${filters.user}&year=${filters.year}&month=${filters.month}`,
		null,
		function (r) {
			r = JSON.parse(r)
			loadrows(r)
			let actions = document.getElementsByClassName("actions")
			for (let action of actions) {
				let timecard = JSON.parse(action.getAttribute("data-timecard"))
				action.querySelector(".modify").onclick = function() { modifyTimecardDialog(timecard) }
				action.querySelector(".delete").onclick = function() { deleteTimecardDialog(timecard) }
			}
		}
	)
}

// No se me ha ocurrido una mejor manera de hacer esto
function updateExports() {
	let f = filters
	let urlencode = `user=${f.user}&year=${f.year}&month=${f.month}`
	var year = {
		"start": f.year ? f.year : "1970",
		"end":   f.year ? f.year : new Date().getFullYear()
	}
	var month = {
		"start": f.month ? f.month.padStart(2,"0") : "01",
		"end":   f.month ? f.month.padStart(2,"0") : "12"
	}
	var start = `${year.start}-${month.start}-01`
	var end = new Date(year.end, month.end, 0)
	end = `${year.end}-${month.end}-${String(end.getDate()).padStart(2,"0")}`
	var range = start + ";" + end

	var pdf = document.getElementById("pdf")
	pdf.href = `/user/${f.user}/download?range=${range}`
	var csv = document.getElementById("csv")
	csv.href = `/editor?download=csv&${urlencode}`
	var json = document.getElementById("json")
	json.href = `/editor?download=json&${urlencode}`
	// TODO 1
}

function filter(f) {
	for (let filter of Object.keys(filters))
		if (f[filter])
			filters[filter] = f[filter] == 0 ? null : f[filter]

	var year = document.getElementById("year")
	var month = document.getElementById("month")
	let check = filters.user == null
	year.disabled = month.disabled = check
	export_view.style.display = check ? "none" : "block"
	if (check) {
		year.selectedIndex = 0
		month.selectedIndex = 0
	}

	reload()
	updateExports()
}

document.getElementById("user").onchange = function() {
	filter({'user': this.value})
}
document.getElementById("year").onchange = function() {
	filter({'year': this.value})
}
document.getElementById("month").onchange = function() {
	filter({'month': this.value})
}

window.onload = function() {
	document.getElementById("user").selectedIndex = 0
	filter(filters)
}
