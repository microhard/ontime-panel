

function addWebhook(when, endpoint) {
	when = encodeURIComponent(when)
	endpoint = encodeURIComponent(endpoint)
	request("POST", "/webhooks", `when=${when}&endpoint=${endpoint}&add`,
		function(response) {
			dialog(
				response,
				[{"name": "Ok", "action": function() { location = location }}]
			)
		}
	)
}
function addWebhookDialog() {
	dialog(
		`<label>
			<h5>Trigger</h5>
			<select name="when">
				<option value="clock-in">When a worker clocks in</option>
				<option value="clock-out">When a worker clocks out</option>
			</select>
		</label>
		<label>
			<h5>Endpoint</h5>
			<input type="text" name="endpoint" placeholder="https://ontime.lander.cat/webhooks">
		</label>`,
		[
			{"name": "Add", "color": "#A3E183", "action": function(form) {
				let when = form.querySelector("[name=when]").value
				let endpoint = form.querySelector("[name=endpoint]").value
				addWebhook(when, endpoint)
			}},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function deleteWebhook(id) {
	id = encodeURIComponent(id)
	request("POST", "/webhooks", `id=${id}&delete`, function(response) {
		dialog(
			response,
			[{"name": "Ok", "action": function() { location = location }}]
		)
	})
}
function deleteWebhookDialog(webhook) {
	dialog(
		`Do you really want to delete this webhook?`,
		[
			{"name": "Accept", "color": "#A3E183", "action": function() { deleteWebhook(webhook.id) }},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}



document.getElementById("add").onclick = addWebhookDialog

let actions = document.getElementsByClassName("actions")
for (let action of actions) {
	let webhook = JSON.parse(action.getAttribute("data-webhook"))
	action.querySelector(".delete").onclick = function() { deleteWebhookDialog(webhook) }
}
