function settingsDialog(email){
	dialog(
		`<h5>Email</h5>
		<input name="email" value="${email}" type="text" placeholder="Your new email">
		<h5>Your Password</h5>
		<input name="password" required type="password" placeholder="Your password">
		<h5>New Password</h5>
		<input name="new_password" type="password" placeholder="Your new password">
		<input name="check_password" type="password" placeholder="Repeat your new password">`,
		[
			{"name": "Send", "color": "#A3E183", "action": function(form) {
				let email = form.querySelector("[name=email]").value
				let password = form.querySelector("[name=password]").value
				let new_password = form.querySelector("[name=new_password]").value
				let check_password = form.querySelector("[name=check_password]").value
				if (new_password == check_password) {
					settings(email, password, new_password)
				} else dialog("The passwords don't match.")
			}},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function settings(email, password, newpassword) {
	email = encodeURIComponent(email)
	password = encodeURIComponent(password)
	newpassword = encodeURIComponent(newpassword)

	request("POST", "/", `email=${email}&password=${password}&newpassword=${newpassword}&settings`, function(response) {
		dialog(response, [{"name":"Ok","action":function(){
			location = location
		}}])
	})
}

document.getElementById("settings").onclick = function(){
	settingsDialog(this.getAttribute("data-email"))
}

