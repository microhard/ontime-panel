
/* EXAMPLES

message: "Test message"
actions: [
	{"name": "First Action",  "color": "red",  "action": function(){}},
	{"name": "Second Action", "color": "blue", "action": function(){}}
]

*/

var dialog_style = document.createElement("link")
dialog_style.type = "text/css"
dialog_style.rel = "stylesheet"
dialog_style.href = "/css/dialog.css"
document.head.appendChild(dialog_style)


function dialog(message, actions = [{"name": "OK"}]) {
	let _dialog = document.getElementById("dialog")
	if (_dialog) {
		document.body.style.overflow = "auto"
		_dialog.remove()
		return
	}
	if (!message) return

	document.body.style.overflow = "hidden"
	_dialog = document.createElement("div")
	_dialog.id = "dialog"
	
	let _message = document.createElement("div")
	_message.innerHTML = message

	let _actions = document.createElement("div")

	for (let action of actions) {
		let button = document.createElement("button");
		button.innerHTML = action.name
		button.style.background = action.color
		button.onclick = function() {
			dialog()
			if (action.action) action.action(_message)
		}
		_actions.appendChild(button)
	}
	_message.appendChild(_actions)
	_dialog.appendChild(_message)
	document.body.appendChild(_dialog)
}
