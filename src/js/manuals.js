let manuals = document.getElementsByClassName('manual')

for (let manual of manuals) {
	manual.onclick = function() {
		this.classList.toggle('open')
	}
}

