function modifyUser(user) {
	let urlencode = new URLSearchParams(user).toString()
	request("POST", "/users", urlencode+"&modify=", function(response) {
		dialog(response)
		let row = document.getElementById("row-token-"+user.token)
		row.children[0].innerHTML = user.name
		row.children[1].innerHTML = user.email
		row.children[2].innerHTML = user.card
	})
}
function modifyUserDialog(user) {
	dialog(
		`<div style="white-space: nowrap">
		<input type="hidden" name="token" value="${user.token}" required>
		<input type="text" name="name" value="${user.name}" placeholder="Full Name" required>
		<input type="email" name="email" value="${user.email}" placeholder="Email" required>
		<input type="text" name="card" value="${user.card}" placeholder="card" required>
		</div>`,
		[
			{"name": "Modify", "color": "#A3E183", "action": function(message){
				let values = message.querySelectorAll("input")
				let user = {}
				for (let value of values)
					user[value.name] = value.value
				modifyUser(user)
			}},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function deleteUser(token) {
	request("POST", "/users", "delete="+token, function(response) {
		dialog(response)
		document.getElementById("row-token-"+token).remove()
	})
}
function deleteUserDialog(user) {
	dialog(
		`Do you really want to delete the user ${user.name}?`,
		[
			{"name": "Accept", "color": "#A3E183", "action": function() { deleteUser(user.token) }},
			{"name": "Cancel", "color": "#E18389"}
		]
	)
}

function sendEmailInstructions(user) {
	request("POST", "/users", "send_instructions="+user.token)
	dialog(`An email will be sent to ${user.name}!`)
}
function sendEmailReport(user) {
	request("POST", "/users", "send_report="+user.token)
	dialog(`An email will be sent to ${user.name}!`)
}
function sendEmailDialog(user) {
	dialog(
		`Which <u>kind of email</u> do you want to send to ${user.name}?
		 <ul>
			<li><b>Instructions:</b> Tell the user how to use OnTime and mark their time with their information.</li>
			<li><b>Report:</b> Send the report of their markings of last month, you can also download older reports in the "Download Reports" tool.</li>
		 </ul>`,
		[
			{"name": "Cancel", "color": "#E18389"},
			{"name": "Send Report", "action": function() { sendEmailReport(user) }},
			{"name": "Send Instructions", "action": function() { sendEmailInstructions(user) }}
		]
	)
}

function search(text) {
	let rows = document.getElementById("rows").children
	for (let row of rows)
		row.style.display = row.textContent.toUpperCase().match(text.toUpperCase()) ? null : "none"
}


document.getElementById("search").onkeyup = function(){
	search(this.value)
}

let actions = document.getElementsByClassName("actions")
for (let action of actions) {
	let user = JSON.parse(action.getAttribute("data-user"))
	action.querySelector(".user").onclick   = function() { window.location = "/user/" + user.token }
	action.querySelector(".email").onclick  = function() { sendEmailDialog(user)  }
	action.querySelector(".modify").onclick = function() { modifyUserDialog(user) }
	action.querySelector(".delete").onclick = function() { deleteUserDialog(user) }
}
