<?php

	require "app.php";

	if (!isset($_SESSION["account"]))
		header("Location: /") and die();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Administration Panel - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<script type="text/javascript" src="/js/request.js"></script>
	<script type="text/javascript" src="/js/settings.js" defer></script>
	<script type="text/javascript" src="/js/dialog.js" defer></script>
</head>
<body>

	<aside>
		<p>Hello, <?php echo $_SESSION["account"]["email"]; ?></p>
		<a href="/">Homepage <i class="fa-solid fa-house"></i></a>
		<a href="/welcome?account_id=<?php echo $_SESSION["account"]["id"] ?>">Welcome <i class="fa-solid fa-id-card-clip"></i></a>
		<a id="settings" data-email="<?php echo $_SESSION["account"]["email"]; ?>">Settings <i class="fa-solid fa-gear"></i></a>
		<a href="/logout">Log Out <i class="fa-solid fa-right-from-bracket"></i></a>
	</aside>

	<main>
		<h1>Administration Panel</h1>

		<section class="columns">
			<div>
				<h2><i class="fa-solid fa-users"></i> Manage Users</h2>
				<a href="/users" class="go">Open tool</a>
				<p>
					Your workers need to be registered into the system before they can start clocking in and out.
					Use this tool to add, delete and modify users that need to mark their time.
				</p>
			</div>
			<div>
				<h2><i class="fa-solid fa-pen-to-square"></i> Edit Markings</h2>
				<a href="/editor" class="go">Open tool</a>
				<p>
					A user forgot to clock in or out? Use this tool if there is any human mistake to quickly fix it!
				</p>
				<p>
					All changes are registered and logged.
				</p>
			</div>
			<div>
				<h2><i class="fa-solid fa-wifi"></i> Set Webhooks</h2>
				<a href="/webhooks" class="go">Open tool</a>
				<p>
					If you want something to happen when somone clocks in or out, we can send you an HTTP request when that happens.
				</p>
			</div>
			<div>
				<h2><i class="fa-solid fa-comments"></i> Get Help </h2>
				<a href="/manuals" class="go">Search manuals</a>
				<a href="/support" class="go">Support page</a>
				<p>
					Need assistance? Please read our manual pages. If you require further assistance or you have a problem with OnTime, please contact us!
				</p>
			</div>
		</section>
	</main>

</body>
</html>