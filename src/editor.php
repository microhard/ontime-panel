<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";

	if (!isset($_SESSION["account"]))
		header("Location: /") and die();


	$DB = new Database();

	function create($data) {
		global $DB;

		$user = $DB->escape($data["user"]);
		$date = $DB->escape($data["date"]);
		$time = $DB->escape($data["time"]);
		$type = $DB->escape($data["type"]);

		if (!$user or !$date or !$time or !$type)
			http_response_code(400) and die();

		$account_id = $_SESSION["account"]["id"];

		$user_check = $DB->query("
			SELECT token
			FROM user
			WHERE token = '$user'
			  AND account_id = $account_id
		");

		if (!$user_check)
			return "An error occurred!";

		$DB->query("
			INSERT INTO timecard (`type`, `timestamp`, `user_token`)
			VALUES ('$type', '$date $time', '$user')
		");

		if ($DB->getAffected() === 0) return "An error occurred!";

		http_response_code(201);
		return "Created marking!";
	}

	function delete($id) {
		global $DB;

		$id = $DB->escape($id);
		if (!$id)
			http_response_code(400) and die("An error occurred!");

		$account_id = $_SESSION["account"]["id"];

		$user = $DB->query("
			SELECT u.token
			FROM timecard t, user u
			WHERE t.user_token = u.token 
			  AND t.id = '$id' 
			  AND u.account_id = '$account_id'
		");

		if (!$user) 
			http_response_code(403) and die("An error occurred!");

		$DB->query("
			DELETE FROM timecard
			WHERE id = '$id'
		");

		return "Deleted succesfully!";
	}

	function modify($timecard) {
		global $DB;

		$id = $DB->escape($timecard["id"]);
		$type = $DB->escape($timecard["type"]);
		$timestamp = $DB->escape($timecard["timestamp"]);
		$user_token = $DB->escape($timecard["user_token"]);

		if (!$id or !$type or !$timestamp or !$user_token)
			http_response_code(400) and die();

		$account_id = $_SESSION["account"]["id"];

		$user_check = $DB->query("
			SELECT token
			FROM user
			WHERE token = '$user_token'
			  AND account_id = $account_id
		");

		if (!$user_check)
			return "An error occurred!";

		$DB->query("
			UPDATE timecard 
			SET `type` = '$type', `timestamp` = '$timestamp'
			WHERE `id` = '$id'
		");

		if ($DB->getAffected() === 0) return "An error occurred!";
		return "Marking modified!";

	}

	function filter($vars) {
		global $DB;

		$user = $DB->escape($vars["user"]);
		$year = $DB->escape($vars["year"]);
		$month = $DB->escape($vars["month"]);

		$year_start = $year == 0 ? 1970 : $year;
		$year_end = $year == 0 ? date("Y") : $year;

		$month_start = $month == 0 ? 1 : $month;
		$month_end = $month == 0 ? 12 : $month;

		$t = strtotime("$year_start-$month_start-1");
		$start_date = date("Y-m-d", $t);
		$t = strtotime("$year_end-$month_end-1");
		$end_date = date("Y-m-t", $t);

		$markings = $DB->query("
			SELECT *
			FROM timecard
			WHERE `user_token` = '$user'
			AND `timestamp` BETWEEN '$start_date' AND '$end_date';
		");

		return $markings;
	}

	function download($data) {
		$type = $data["download"];
		switch ($type) {
			case "csv":
				header("Content-Type: text/csv");
				$csv = "id,type,timestamp,user_token\n";
				foreach (filter($data) as $marking)
					$csv .= implode(",", $marking)."\n";
				return $csv;
			
			case "json":
				header("Content-Type: application/json");
				return json_encode(filter($data));

			default:
				break;
		}
	}


	$users = $DB->query(
		"SELECT * FROM user WHERE account_id = ".$_SESSION["account"]["id"]." ORDER BY name"
	);
	$users = $users ? $users : array();

	$alert = false;

	if (isset($_POST["create"]))
		$alert = create($_POST);
	elseif (isset($_POST["delete"]))
		die(delete($_POST["delete"]));
	elseif (isset($_POST["modify"]))
		die(modify($_POST));
	elseif (isset($_GET["download"]))
		die(download($_GET));
	elseif (isset($_GET["user"]))
		die(json_encode(filter($_GET)));

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Edit Markings - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="stylesheet" type="text/css" href="/css/table.css">

	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">

	<script type="text/javascript" src="/js/dialog.js" defer></script>
	<script type="text/javascript" src="/js/request.js"></script>
	<script type="text/javascript" src="/js/editor.js" defer></script>
</head>
<body>

	<form method="POST" action="/editor">
		<?php if ($alert) echo "<code class='alert'>$alert</code>" ?>
		<select required name="user">
			<option>Select a user...</option>
			<?php foreach ($users as $user): ?>
				<option value="<?php echo $user["token"] ?>">
					<?php echo $user["name"] ?>
				</option>
			<?php endforeach ?>
		</select>
		<input type="date" name="date" required>
		<input type="time" name="time" required>
		<div>
			<label>
				<input type="radio" name="type" value="IN" checked>
				IN
			</label>
			<label>
				<input type="radio" name="type" value="OUT">
				OUT
			</label>
		</div>
		<button name="create">
			<i class="fa-solid fa-calendar-plus"></i>
			Generate Marking
		</button>
	</form>

	<main>
		<a href="/panel" id="back">Go back to the administration panel</a>
		<h1>Edit User Markings</h1>
		<select id="user">
			<option value="0">Select a user...</option>
			<?php foreach ($users as $user): ?>
				<option value="<?php echo $user["token"] ?>">
					<?php echo $user["name"] ?>
				</option>
			<?php endforeach ?>
		</select>
		<select id="year" disabled>
			<option value="0">Select a year...</option>
			<?php for ($i = 2020; $i <= date("Y"); $i++): ?>
				<option value="<?php echo $i ?>"><?php echo $i ?></option>
			<?php endfor ?>
		</select>
		<select id="month" disabled>
			<option value="0">Select a month...</option>
			<option value="1">January</option>
			<option value="2">February</option>
			<option value="3">March</option>
			<option value="4">April</option>
			<option value="5">May</option>
			<option value="6">June</option>
			<option value="7">July</option>
			<option value="8">August</option>
			<option value="9">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		</select>
		<hr>
		<table id="markings">
			<thead>
				<tr>
					<th>Type</th>
					<th>Timestamp</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody id="rows"></tbody>
		</table>
		<hr>
		<div id="export">
			<a id="csv" download="markings.csv">
				<i class="fa-solid fa-file-csv"></i> Export as CSV
			</a>
			<a id="json" download="markings.json">
				<i class="fa-solid fa-file-code"></i> Export as JSON
			</a>
			<a id="pdf">
				<i class="fa-solid fa-file-pdf"></i> Export as PDF
			</a>
		</div>
	</main>

</body>
</html>