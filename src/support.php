<?php

	require "app.php";
	include __ROOT__."/utils/mail/mail.php";


	if (isset($_POST["message"])) {

		if (isset($_POST["email"]))
			$requestor = $_POST["email"];
		else
			$requestor = $_SESSION["account"]["email"];

		if (!$requestor)
			http_response_code(400) and die();

		$sent = \mail\send(
			"[OnTime Support] " . $requestor,
			\mail\fill_template(
				\mail\TEMPLATE_SUPPORT,
				array(
					"account" => $requestor,
					"message" => htmlspecialchars($_POST["message"])
				)
			),
			//"ontime.panel@gmail.com"
			"facundo.huetagoyena.7e4@itb.cat"
		);

		if (!$sent)
			die("An error occurred! Please try again later.");

		die("We will review your inquiry and contact you back as soon as possible!");

	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Support - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<link rel="stylesheet" type="text/css" href="/css/support.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />

	<script type="text/javascript" src="/js/request.js"></script>
	<script type="text/javascript" src="/js/dialog.js"></script>
	<script type="text/javascript" src="/js/support.js" defer></script>
</head>
<body>


	<main>
		<a href="/panel" id="back">Go back to the main page</a>
		<h1>Contact us</h1>

		<p>Send us any questions or concerns you may have.</p>
		<?php if (isset($_SESSION["account"])): ?>
		<p>We will send an email to <i><?php echo $_SESSION["account"]["email"] ?></i> when we find a solution to your concern.</p>
		<?php endif ?>

		<form method="POST">
			<?php if (!isset($_SESSION["account"])): ?>
			<p>What is your mail address?</p>
			<input type="email" name="email" placeholder="john@doe.us">
			<?php endif ?>
			<p>What is the problem?</p>
			<textarea name="message" placeholder="Write here..."></textarea>
			<button>Send</button>
		</form>
	</main>


</body>
</html>