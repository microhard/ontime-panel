<?php

	require "app.php";
	include __ROOT__."/lib/Database.php";

	function changeSettings() {
		$DB = new Database();
		$account = $_SESSION["account"];
		$email = $DB->escape($_POST["email"]);
		$password = $_POST["password"];
		$newpassword = $_POST["newpassword"];

		if (!$email or !$password or $newpassword === null)
			http_response_code(400) and die();

		if ($newpassword === $password) 
			return "It can't be the same password as before";

		if (!password_verify($password, $account["password"])) 
			return "Incorrect password";

		if ($newpassword) {
			$newpassword = password_hash($newpassword, PASSWORD_BCRYPT);
			$DB->query("UPDATE account SET password='$newpassword' WHERE email='$account[email]'");
		}

		if ($email !== $account["email"]) {
			$DB->query("UPDATE account SET email='$email' WHERE email='$account[email]'");
			$_SESSION["account"]["email"] = $email;
		}

		return "Settings saved!";
	}

	if (isset($_POST["settings"]) and isset($_SESSION["account"]))
		die(changeSettings());

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Administration Panel - On Time</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" href="/favicon.svg" sizes="any" type="image/svg+xml">
	<link href="/css/font/fontawesome/css/fontawesome.css" rel="stylesheet" />
	<link href="/css/font/fontawesome/css/solid.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="/css/form.css">
	<script type="text/javascript" src="/js/dialog.js" defer></script>
	<script type="text/javascript" src="/js/settings.js" defer></script>
	<script type="text/javascript" src="/js/request.js"></script>
</head>
<body>

	<aside>
		<?php if (isset($_SESSION["account"])): ?>		
		<p>Hello, <?php echo $_SESSION["account"]["email"]; ?></p>
		<a href="/panel">Administration Panel <i class="fa-solid fa-grip-vertical"></i></a>
		<a id="settings" data-email="<?php echo $_SESSION["account"]["email"]; ?>">Settings <i class="fa-solid fa-gear"></i></a>
		<?php else: ?>
		<p>Welcome to the On Time Administration Panel</p>
		<a href="/login">Log In <i class="fa-solid fa-right-to-bracket"></i></i></a>
		<a href="/register">Register <i class="fa-solid fa-circle-plus"></i></a>
		<?php endif ?>
		<a href="/manuals">Manuals <fa class="fa-solid fa-book"></fa></a>
		<a href="/support">Support <fa class="fa-solid fa-circle-question"></fa></a>
		<?php if (isset($_SESSION["account"])): ?>
		<a href="/logout">Log Out <i class="fa-solid fa-right-from-bracket"></i></a>
		<?php endif ?>
	</aside>

	<main>
		<h1>On Time Administration Panel</h1>

		<h2><i class="fa-solid fa-gavel"></i> The Law</h2>

		<div class="columns">
			<a href="https://ec.europa.eu/social/main.jsp?catId=706&langId=en&intPageId=205">EU Working Time Directive</a>
			<a href="https://boe.es/buscar/doc.php?id=BOE-A-2019-3481">Spanish Law on Punch Clocks (Art.10)</a>
		</div>

		<p>
			These are some of the regulations relating working time tracking. It is important for workers and employers to have a record of work time.
		</p>

		<h2><i class="fa-solid fa-triangle-exclamation"></i> The Problem</h2>
		<p>
			Modern companies start in a garage, and people in garages don't care about the law, that is why compliance is always at the bottom of the task list.
		</p>
		<p>
			We believe compliance is important in society, and that organizations <b>must</b> follow these regulations, and even the guidelines set by competent authorities. This is almost never the case, companies rely on the solutions that require the least amount of time and work, and this usually leads to errors and human mistakes on something as important as the safety of workers.
		</p>
		<p>
			It is very usual for companies to develop the absolute minimum in terms of compliance. Workers rely on low quality applications that are usually rushed by developers, and this is a huge problem.
		</p>

		<h2><i class="fa-solid fa-clipboard-check"></i> The Solution</h2>
		<p>
			On Time solves this problem in the following ways:
			<ul>
				<li>
					<strong>Free and Open Source Software</strong>: Any organization can use this tool and even install it on their own servers for no cost.
				</li>
				<li>
					<strong>Fully compliant</strong>: Unlike rushed applications, On Time is made to be an easy tool to comply with the regulations in all European Union member states.
				</li>
				<li>
					<strong>A lot of features</strong>: When an application is made only to be compliant, this lacks features and is usually just a load for the workers instead of a useful tool. On Time has a lot of features both for the workers and employers.
				</li>
				<li>
					<strong>It's Cooler than your current application</strong>: If you already have a in-house solution we can still convince you to use our application. On Time looks better than any rushed application and this helps improve your brand's image and worker morality.
				</li>
			</ul>
		</p>
	</main>

</body>
</html>